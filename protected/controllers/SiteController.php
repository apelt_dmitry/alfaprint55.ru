<?php

class SiteController extends Controller
{

	public function actionIndex()
	{
            $model = Client::model()->findAll();
            $this->pageTitle = 'Главная';
            $this->render('index', array(
                'model' => $model,
            ));

	}
        

        public function actionPrices() 
        {
            $model = PriceCategory::model()->findAll();
            $this->pageTitle = 'Цены';
            $this->render('prices', array(
                'model' => $model,
            ));
        }
       
        
        public function actionServices()
        {
            $model = Services::model()->findAll();
            $this->pageTitle = 'Услуги';
            $this->render('services', array(
                'model' => $model,
            ));
        }
        
        public function actionContacts() 
        {
            $model = Contacts::model()->findAll();
            $this->pageTitle = 'Контакты';
            $this->render('contacts', array(
                'model' => $model,
            ));
        }
        
        public function actionNews()
	{
            $model = News::model()->findAll();
            $this->pageTitle = 'Новости';
            $this->render('news', array(
                'model' => $model,
            ));
	}
        
        public function actionReport()
	{
            $model = Report::model()->findAll();
            $this->pageTitle = 'Фотоотчёты';
            $this->render('report', array(
                'model' => $model,
            ));
	}
        
        public function actionPortfolio()
        {
            $model = Portfolio::model()->findAll();
            $servicesModel = Services::model()->findAll();
            $this->pageTitle = 'Портфолио';
            $this->render('portfolio', array(
                'model' => $model,
                'servicesModel' => $servicesModel,
            ));
        }
        
        public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

        
        public function actionPage($id)
        {
            $model = Menu::model()->findByPk($id);
            if ( !$model ) {
                $this->redirect(Yii::app()->homeUrl);
            }
            
            $this->render('page', array(
                'model'=>$model,
            ));
        }
        
        
        public function actionSearch()
        {
            //$_POST['query']
            $this->render('search', array(
                'model'=>$model,
            ));
        }
        
        public function actionContact()
        {
            $model = new ContactForm();

            if ( isset($_POST['ajax']) && $_POST['ajax']==='contact-form' ) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if ( isset($_POST['ContactForm']) ) {
                $model->attributes = $_POST['ContactForm'];
                if ( $model->validate() ) {
                    $model->goContact();
                    echo '1';
                } else {
                    echo '0';
                }
            }
        }
        
        public function actionOrder()
        {
            $model = new OrderForm();

            if ( isset($_POST['ajax']) && $_POST['ajax']==='order-form' ) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if ( isset($_POST['OrderForm']) ) {
                $model->attributes = $_POST['OrderForm'];
                $model->services = $_POST['services'];
                $model->file= CUploadedFile::getInstance($model, 'file');
                if ($model->validate()) {
                    $model->goOrder();
                    echo '1';
                } else {
                    echo '0';
                }
            }
            $this->redirect(array('site/index'));
        }

}