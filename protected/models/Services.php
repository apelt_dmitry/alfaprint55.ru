<?php

/**
 * This is the model class for table "service".
 *
 * The followings are the available columns in table 'service':
 * @property integer $id
 * @property string $name
 * @property string $img
 * @property string $text
 * @property string $text_promo_1
 * @property string $text_promo_2
 */
class Services extends CActiveRecord
{
        public $image;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'service';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, text, text_promo_1, text_promo_2', 'required'),
			array('name', 'length', 'max'=>128),
			array('img, text_promo_1, text_promo_2', 'length', 'max'=>64),
                        array('img', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
                        array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true),
			array('id, name, img, text, text_promo_1, text_promo_2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
                        'image' => 'Изображение',
			'id' => 'ID',
			'name' => 'Имя',
			'img' => 'Изображение',
			'text' => 'Контент',
			'text_promo_1' => 'Промо 1',
			'text_promo_2' => 'Промо 2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('text_promo_1',$this->text_promo_1,true);
		$criteria->compare('text_promo_2',$this->text_promo_2,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Service the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function get_dropDownList(){
            $result = array();
            $model = $this->findAll();  
            foreach ($model as $val){
                $result[$val->id] = $val->name;
            }
            return $result;
        }
}
