<?php

class OrderForm extends CFormModel 
{
    public $name;
    public $phone;
    public $email;
    public $message;
    public $company_name;
    public $activities_of_the_company;
    public $file;
    public $services;


    public function rules()
    {
        return array(
            array('name, phone, email, message','required'),
            array('name, company_name, activities_of_the_company, email', 'length', 'max'=>64),
            array('message', 'length', 'max'=>1024),
            array('email','email'),
            array('phone','length', 'max'=>16),
            array('file', 'file', 'allowEmpty'=>true/*,'maxFiles'=>12*/),
            //array('phone', 'match'/*'pattern'=>'/\d \(\)+\-/ui'*/),
            array('services','safe'),
            array(
                'activities_of_the_company',
                'required',
                'message'=>'Введите род деятельности'
                ),
            array(
                'company_name',
                'required',
                'message'=>'Введите название компании'
                ),
            
            );
        
        
    }


    public function attributeLabels()
    {
        return array(
            'name'=>'Контактное лицо',
            'phone'=>'Телефон',
            'email'=>'E-mail',
            'message'=>'Текст сообщения',
            'activities_of_the_company'=>'Род деятельности компании',
            'company_name'=>'Название компании'
        );
    }
    
    public function goOrder()
    {
        include_once "libmail.php";
        $m = new Mail;
        $m->From($this->email.';admin@mir-it.info');
        $m->To(array(Yii::app()->controller->config->adminEmail));
        $m->Subject('Тема письма');
        $this->services;
        $serv = explode(",", $this->services);
        $model=Services::model()->findAllByPk($serv);
        foreach ($model as $row){
            $services .= $row->name.', '; 
        }
        $m->Body(''
                //. 'Получаемый файл: '.$this->file->tempName.'. '
                . 'Отправитель: '.$this->name.'. '
                . 'Телефон: '.$this->phone. '. '
                . 'Сообщение: '.$this->message. '. '
                . 'Род деятельности компании: '.$this->activities_of_the_company. '. '
                . 'Название компании: '.$this->company_name. '. '
                . 'Выбранные услуги: '.$services.'.');
        if (strlen($this->file->tempName)!=0) {
            $m->Attach($this->file->tempName, $this->file->name, null,"attachment");
        }
        $m->Send();
    }
    
    
}
