<?php

/**
 * This is the model class for table "portfolio".
 *
 * The followings are the available columns in table 'portfolio':
 * @property integer $id
 * @property integer $category
 * @property string $img
 */
class Portfolio extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public $image;

    public function tableName()
    {
        return 'portfolio';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('service_id', 'required'),
            array('service_id', 'numerical', 'integerOnly' => true),
            array('image', 'file', 'types' => 'jpg, gif, png', 'allowEmpty' => true, 'maxFiles' => 12),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, service_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'portfolioSlide' => array(self::HAS_MANY, 'PortfolioSlide', 'portfolio_id'),
            'services' => array(self::BELONGS_TO, 'Services', 'service_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'image[]' => 'Изображение',
            'id' => 'ID',
            'service_id' => 'Категория',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Portfolio the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function get_preview()
    {
        $slides = $this->portfolioSlide;
        if (count($slides) == 0) {
            return NULL;
        } else {
            return BsHtml::image('/uploads/portfolio/preview/' . $slides[0]->img);
        }
    }
}
