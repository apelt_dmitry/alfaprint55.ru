<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    // bootstrap 3.3.1
    ->registerCssFile($pt.'css/bootstrap.min.css')
    // bootstrap theme
    ->registerCssFile($pt.'css/bootstrap-theme.min.css')
    ->registerCssFile($pt.'css/main.css');

$cs
    ->registerCoreScript('jquery',CClientScript::POS_END)
    ->registerCoreScript('jquery.ui',CClientScript::POS_END)
    ->registerCoreScript('cookie',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/bootstrap.min.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/main.js');
$cs
    ->registerScriptFile($pt.'js/tubeutil.js')
    ->registerScriptFile($pt.'js/init.js');

$config = Config::model()->findByPk(1);
?>


<!DOCTYPE html>
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
	<title><?= Yii::app()->name;?></title>
</head>

<body>

	<?php //echo $content; ?>

    <div class="container">
        <div class="row">
           <div class="col-md-12">
               <legend>
                   <?= Yii::app()->name;?>
               </legend>
           </div>
        </div>

        <div class="row">
            <?php
                $model = new SearchForm();
                $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                    'layout' => BsHtml::FORM_LAYOUT_INLINE,
                    'enableAjaxValidation' => true,
                    'id' => 'user_form_inline',
                    'htmlOptions' => array(
                        'class' => 'blocks'
                    )
                ));
            ?>

            <fieldset>
                <div style="position: relative">
                    <?= $form->textFieldControlGroup($model, 'query', array(
                        'style'=>'width:100%',
                        'class'=>'search',
                        'AUTOCOMPLETE'=>'off',
                        'groupOptions'=>array('class'=>'col-md-9'),
                    )); ?>
                    <ul class="typeahead dropdown-menu reset autocomplete" style="top:35px;left:15px;">
                        <li>
                            <a href="#">blabla</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <?= BsHtml::submitButton('Найти', array(
                        'class'=>'btn',
                        'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                        'block'=>true,
                    )); ?>

                </div>

            </fieldset>
            <?php $this->endWidget();?>

            <!--<div class="col-md-9">1</div>
            <div class="col-md-3">2</div>-->
        </div>

        <div class="row margin-top" id="for_100">
            <div class="col-md-2">
                <?= $config->banner1 ?>
            </div>

            <div class="col-md-8" id="tube_video">
                <?= $config->welcome ?>
                <?= $content ?>
            </div>

            <div class="col-md-2">
                <?= $config->banner2 ?>
            </div>
        </div>

        <div class="row margin-top">
            <div class="col-md-2">

            </div>

            <div class="col-md-8">
                <!-- Put this script tag to the <head> of your page -->
				<script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>

				<script type="text/javascript">
				  VK.init({apiId: 4741763, onlyWidgets: true});
				</script>

				<!-- Put this div tag to the place, where the Comments block will be -->
				<div id="vk_comments"></div>
				<script type="text/javascript">
				VK.Widgets.Comments("vk_comments", {limit: 10, width: "665", attach: "*"});
				</script>
            </div>

            <div class="col-md-2">

            </div>
        </div>
    </div>
</body>
</html>
