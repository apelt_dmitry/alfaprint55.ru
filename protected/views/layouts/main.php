<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    // bootstrap 3.3.1
    ->registerCssFile($pt.'css/bootstrap.min.css')
    // bootstrap theme
    ->registerCssFile($pt.'css/bootstrap-theme.min.css')

    ->registerCssFile($pt.'css/jqx.base.css')
    
    ->registerCssFile($pt.'css/jqx.arctic.css')
    //->registerCssFile($pt.'css/site_global.css')
        
    //->registerCssFile($pt.'css/index.css')
    ->registerCssFile($pt.'css/animate.css')
    ->registerCssFile($pt.'css/main.css');

$cs
    
    ->registerCoreScript('jquery',CClientScript::POS_END)
    ->registerCoreScript('jquery.ui',CClientScript::POS_END)
    ->registerCoreScript('cookie',CClientScript::POS_END)
    //->registerScriptFile($pt.'js/bootstrap.min.js',CClientScript::POS_END)
    //->registerScriptFile($pt.'js/jquery-1.8.2.min.js')
    //->registerScriptFile($pt.'js/ddl/jquery-1.11.1.min.js')

    ->registerScriptFile($pt.'js/ddl/demos.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/ddl/jqxcore.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/ddl/jqxbuttons.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/ddl/jqxscrollbar.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/ddl/jqxlistbox.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/ddl/jqxdropdownlist.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/ddl/jqxcheckbox.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/ddl/jqxdata.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/jquery.carouFredSel-6.0.4-packed.js',CClientScript::POS_END)
    ->registerScriptFile($pt.'js/main.js',CClientScript::POS_END);
    

    /*$result = array(
        //array('label'=>'Главная', 'url'=>array('site/index')),
    );
    $criteria = new CDbCriteria();
    $criteria->select = array('id', 'name', 'parent_id');
    $criteria->order = '`parent_id` ASC, `z_index` ASC';
    $menu = Menu::model()->findAll($criteria);
    foreach ( $menu as $row ) {
        if ( $row->parent_id==0 ) {
            $result[$row->id] = array('label'=>$row->name, 'url'=>array('site/page', 'id'=>$row->id));
        } else {
            $result[$row->parent_id]['items'][] = array('label'=>$row->name, 'url'=>array('site/page', 'id'=>$row->id));
            $result[$row->parent_id]['itemOptions'] = array('class' => 'parent');  
        }
    }*/
    $config=$this->config;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="ru">
        <meta name="robots" content="index, follow">
        <meta name="keywords" content="<?= $config->meta_keys ?>">
        <meta name="title" content="<?= $config->title ?>">
        <meta name="description" content="<?= $config->meta_description ?>">
        <title><?php echo $this->pageTitle; ?></title>
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/png">
        
        <script type="text/javascript">
            $(function() {

                //	create carousel
                $('#carousel').carouFredSel({
                        responsive: true,
                        items: {
                              //  width: //120,
                              //  height: //'60%',
                                visible: 5
                        },
                        auto: {
                                items: 1
                        },
                        prev: '#prev',
                        next: '#next'
                });

                //	re-position the carousel, vertically centered


            });
               
            
        </script>
  
    </head>

    <body>
       
        <div class="header-line">
            <div class="container">
                <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'id'=>'main_menu',
                        'items'=>array(
                            array('label'=>'Главная', 'url'=>array('site/index'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Услуги', 'url'=>array('/site/services','id'=>1),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Цены', 'url'=>array('site/prices'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>  CHtml::image('/img/logo.png'),'url'=>array('site/index'),'itemOptions'=> array('id'=>'logo_img','class'=>'swing animated')),
                            array('label'=>'Работы','url'=>array('site/report'), 'items'=>array(
                                    array('label'=>'Фотоотчеты', 'url'=>array('site/report')),
                                    array('label'=>'Портфолио', 'url'=>array('site/portfolio'))
                                ), 
                                'itemOptions'=> array('class'=>'li_menu')
                            ),
                            array('label'=>'Новости', 'url'=>array('site/news'),'itemOptions'=> array('class'=>'li_menu')),
                            array('label'=>'Контакты', 'url'=>array('site/contacts'),'itemOptions'=> array('class'=>'li_menu')),
                        ),
                        'htmlOptions'=>array(
                            'class'=>'',
                            
                        ),
                        'activateParents'=>true,
                        'encodeLabel'=>false,
                    ));
                ?>
            </div>
        </div>
        
        <?= $content ?>
        
        <div class="footer">
            <div class="red_line3">
                <div class="left_hand"animate='rotateInDownLeft'></div>
                <div class="right_hand"animate='rotateInDownRight'></div>
                <div class="footer_text1">
                        Вы хотите сделать заказ или задать вопрос?
                </div>
                <a class="footer_text2" href="#" onclick="$('#orderDialog').dialog('open'); return false;">
                    Сделать заказ
                </a>
                <a class="footer_text2 footer_text3" href="#" onclick="$('#contactDialog').dialog('open'); return false;">
                    Задать вопрос
                </a>
                <div class="red_line_top">
                </div> 
                <div class="red_line_bottom">
                </div> 
            </div>
            <div>
                <div class="red_line4">
                </div> 
                <div class="container">
                    <div class="container2">
                        <div id="ym">
                            <!-- Yandex.Metrika informer -->
                            <a href="https://metrika.yandex.ru/stat/?id=28806436&amp;from=informer"
                            target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/28806436/3_0_C74141FF_A72121FF_0_pageviews"
                            style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:28806436,lang:'ru'});return false}catch(e){}"/></a>
                            <!-- /Yandex.Metrika informer -->

                            <!-- Yandex.Metrika counter -->
                            <script type="text/javascript">
                            (function (d, w, c) {
                                (w[c] = w[c] || []).push(function() {
                                    try {
                                        w.yaCounter28806436 = new Ya.Metrika({id:28806436,
                                                clickmap:true,
                                                trackLinks:true,
                                                accurateTrackBounce:true});
                                    } catch(e) { }
                                });

                                var n = d.getElementsByTagName("script")[0],
                                    s = d.createElement("script"),
                                    f = function () { n.parentNode.insertBefore(s, n); };
                                s.type = "text/javascript";
                                s.async = true;
                                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                                if (w.opera == "[object Opera]") {
                                    d.addEventListener("DOMContentLoaded", f, false);
                                } else { f(); }
                            })(document, window, "yandex_metrika_callbacks");
                            </script>
                            <noscript><div><img src="//mc.yandex.ru/watch/28806436" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
                            <!-- /Yandex.Metrika counter -->
                        </div>
                        <div class="footer_mir_it">
                            <div class="created_by">created by</div>
                            <a class="logo_footer" style="float: left; margin-top: 10px;" href="http://mir-it.info/"><img src="/img/logo_footer.png" alt=""></a>
                            <div class="created_by">&</div>
                            <a class="logo_footer_2" href="#"><img src="/img/logo_footer_2.png" alt=""></a> 
                        </div>
                        <div class="footer_box1">
                            <div class="city_info">
                                г.Омск, ул. Кемеровская, 1а
                            </div>
                            <div class="phone1">

                            </div>
                            <div class="phone_number">
                                +7(951) 404-58-88
                            </div>
                            <div class="phone2">

                            </div>
                            <div class="phone_number">
                                (3812) 79-96-94, 34-14-28
                            </div>
                            <div class="email_img">

                            </div>
                            <div class="email">
                                <?= BsHtml::link('alfa-zakaz@inbox.ru', 'mailto:alfa-zakaz@inbox.ru', array('target'=>'_blank')) ?>
                            </div>
                        </div>
                        <div class="punktir"></div>
                        <div class="footer_box2">
                            <a href="/site/news"><div class="news"animate='rotateIn'>
                                Наш новостной блог
                                <div class="microfhone">
                                </div>
                            </div></a>
                            <a href="https://vk.com/alfaprint55" class="vk" animate='bounceIn'>
                            </a>
                            <a href="https://instagram.com/alfaprint55/" class="instagram" animate='bounceIn'>
                            </a>
                        </div>
                        <div class="punktir"></div>
                        <div class="footer_box3">
                            <div class="fly_men">
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>


        <?php
            $orderForm = new OrderForm;
        ?>
        <?php
            $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                'id'=>'orderDialog',
                // additional javascript options for the dialog plugin
                'options'=>array(
                    'draggable'=>false,
                    'resizable'=>false,
                    'modal'=>true,
                    'autoOpen'=>false,
                    'width'=>'780',
                    'height'=>'590',
                    'open'=>'js:function(){$(\'.ui-widget-overlay\').click(function(){
                        $(\'#orderDialog\').dialog(\'close\');
                    })}',
                ),
            )); 
        ?>
        <?php $form = $this->beginWidget('CActiveForm', array(
                'id'=>'order-form',
                'action'=>array('/site/order'),
                'enableAjaxValidation'=>FALSE,
                'enableClientValidation'=>true,
                'htmlOptions'=>array(
                    'enctype'=>'multipart/form-data',
                ),
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    /*'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#order-form').attr('action'),
                                data: $('#order-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#order-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                    } else {
                                        alert('Ошибка!!!');
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",*/
                ),
            )); ?>
        <div class="dialog_title">Оформление заказа</div>
                <?= $form->textField($orderForm,'name', array(
                    'placeHolder'=>'Контактное лицо',
                )); ?>
                <?= $form->error($orderForm,'name'); ?>
        
                <?= $form->textField($orderForm,'company_name', array(
                    'placeHolder'=>'Название компании',
                )); ?>
                <?= $form->error($orderForm,'company_name'); ?>
        
                <?= $form->textField($orderForm,'activities_of_the_company', array(
                    'placeHolder'=>'Род деятельности компании',
                )); ?>
                <?= $form->error($orderForm,'activities_of_the_company'); ?>
        
                <?= $form->textField($orderForm,'phone', array(
                    'placeHolder'=>'Телефон',
                )); ?>
                <?= $form->error($orderForm,'phone'); ?>
        
                <?= $form->textField($orderForm,'email', array(
                    'placeHolder'=>'E-mail',
                )); ?>
                <?= $form->error($orderForm,'email'); ?>
        
                <div id="order_form_message_label"><?= $form->label($orderForm, 'message') ?></div>
                <?= $form->textArea($orderForm,'message', array(
                    //'placeHolder'=>'Ваше сообщение',
                    'resize'=>'false',
                )); ?>
                <?= $form->error($orderForm,'message'); ?>
                <div id="order_checkbox_title">Какие услуги вас интересуют</div>
                <div id="order_checkbox">
                    <?php 
                        $servicesModel = Services::model()->findAll();
                    ?>
                    <script type="text/javascript">
                         $(document).ready(function () {                
                            var data = [
                                    {value:'0',label:'ВСЕ'},
                                    <?php foreach ($servicesModel as $row):?>
                                    {value:'<?= $row->id ?>',label:'<?= $row->name ?>'},
                                    <?php endforeach;?>
                                ];
                                $("#jqxDropDownList").jqxDropDownList({ 
                                    checkboxes: true,
                                    source: data,
                                });
                                setTimeout(function(){
                                    $('#jqxDropDownList input').attr('name','services');
                                }, 1000);
                                $("#jqxDropDownList").on('checkChange', function (event){
                                        if (event.args) {
                                        var item = event.args.item;
                                        var value = item.value;
                                        var label = item.label;
                                        var checked = item.checked;
                                        var checkedItems = $("#jqxDropDownList").jqxDropDownList('getCheckedItems');
                                        if (value==0){
                                            $("#jqxDropDownList").jqxDropDownList('checkAll');
                                        }
                                        console.log(value);
                                    }
                                });
                                
                            // Create a jqxDropDownList
                            
                            // subscribe to the checkChange event.

                        });
                    </script>
                    <div>
                        <div style='float: left;' id='jqxDropDownList'>
                        </div>
                        <div style="float: left; margin-left: 20px; font-size: 13px; font-family: Verdana;">
                            <div id="selectionlog"></div>
                            <div style='max-width: 300px; margin-top: 20px;' id="checkedItemsLog"></div>
                        </div>

                    </div>
                </div>
        
                <div id='order_dialog_button'>
                    <?= CHtml::submitButton('Отправить заказ', array(
                    'class'=>'dialog_button',
                    )) ?>
                </div>
                
                <div class="order_footer">
                    <div id="pazaza">* Пожалуйста заполните все белые поля:  Контактное лицо ;  Телефон; E-mail;</div>
                    <div id="plz">Приложить файл:</div>
                    <div id="brif">К вашей заявке вы можете приложить бриф и ТЗ <br/>на разработку макета или другие материалы </div>
                    <a href="#" id="download_file"></a>
                </div>
                <?= $form->fileField($orderForm,'file',array(
                    'style'=>'display:none',
                    )); 
                ?>
                <script>
                    $(document).ready(function(){
                        $('#download_file').click(function(){
                            $('#OrderForm_file').click();
                            return false;
                        });
                    });
                </script>

        <?php $this->endWidget(); ?>
        <?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
                
                
                
        <?php
            $contactForm = new ContactForm;
        ?>
        <?php
            $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                'id'=>'contactDialog',
                // additional javascript options for the dialog plugin
                'options'=>array(
                    'draggable'=>false,
                    'resizable'=>false,
                    'modal'=>true,
                    'title'=>'Задать вопрос',
                    'autoOpen'=>false,
                    'width'=>'780',
                    'height'=>'590',
                    'open'=>'js:function(){$(\'.ui-widget-overlay\').click(function(){
                        $(\'#contactDialog\').dialog(\'close\');
                    })}',
                ),
            )); 
        ?>
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id'=>'contact-form',
                'action'=>array('/site/contact'),
                'enableAjaxValidation'=>true,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnChange'=>false,
                    'validateOnSubmit'=>true,
                    'afterValidate' => "js: function(form, data, hasError) {
                        if ( !hasError) {
                            $.ajax({
                                type: 'POST',
                                url: $('#contact-form').attr('action'),
                                data: $('#contact-form').serialize(),
                                success: function(data_inner) {
                                    if ( data_inner==1 ) {
                                        $('#contact-form').html('<div class=\"dialog_title\">Ваше сообщение отправлено нашему менеджеру. Спасибо!<br>Мы свяжемся с Вами в течение рабочего дня.</div>');
                                    } else {
                                        alert('За вами выехали!!!');
                                        location='http://ru.pornhub.com';
                                    }
                                }
                            });
                        }
                        return false;
                    }
                    ",
                ),
            )); ?>

                <div class="dialog_title">Ваш вопрос</div>
                <?= $form->textField($contactForm,'name', array(
                    'placeHolder'=>'Контактное лицо',
                )); ?>
                <?= $form->error($contactForm,'name'); ?>
        
                <?= $form->textField($contactForm,'phone', array(
                    'placeHolder'=>'Телефон',
                )); ?>
                <?= $form->error($contactForm,'phone'); ?>
        
                <?= $form->textField($contactForm,'email', array(
                    'placeHolder'=>'E-mail',
                )); ?>
                <?= $form->error($contactForm,'email'); ?>
        
                <?= $form->label($contactForm, 'message') ?>
                <?= $form->textArea($contactForm,'message', array(
                    //'placeHolder'=>'Ваше сообщение',
                    'resize'=>'false',
                )); ?>
                <?= $form->error($contactForm,'message'); ?>
        
                <?= CHtml::submitButton('Отправить вопрос', array(
                    'class'=>'dialog_button',
                )) ?>
                
                <div class="dialog_footer">* Пожалуйста заполните все белые поля: Контактное лицо ;  Телефон; E-mail;</div>

            <?php $this->endWidget(); ?>
        <?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
    
    </body>
</html>
