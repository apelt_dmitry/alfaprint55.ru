<?php
    $this->widget('application.extensions.fancybox.EFancyBox', array(
        'target'=>'a.gallery',
    ));
?>
<div class="report_background">
    <div class="report_camera" animate='fadeInLeft'></div>
    <div class="report_photo_1"animate='rollIn'></div>
    <div class="report_photo_2"animate='rollIn'></div>
    <div class="report_cameras"animate='flash'></div>
    <div class="report_slogan1">наши</div>
    <div class="report_slogan2">Фотоотчеты</div>
    <div class="report_line"></div> 
    <div class="report_page"animate='rotateIn'></div>
</div>
<div class="report_body">
    <div class="container">
        <?php foreach ($model as $row): ?>
            <div class="report_box">
                <div class="report_img" style="background: url('/uploads/report/<?=$row->preview ?>') center no-repeat;" ></div>
                <div class="report_title"><?=$row->title ?></div>
                <div class="report_description"><?=$row->description ?></div>
                <?php 
                    $firstSlide=true;
                    $reportSlide=$row->reportSlide;
                ?>
                <?php foreach ($reportSlide as $slide):?>
                    <?php if($firstSlide): ?>
                        <?php $firstSlide=false;?>
                        <a class="gallery report_button" rel="report_<?= $row->id ?>" href="/uploads/report/<?= $slide->img ?>">Посмотреть<div class="report_triangle"></div></a>
                    <?php else: ?>
                        <a class="gallery" rel="report_<?= $row->id ?>" href="/uploads/report/<?= $slide->img ?>" style="display: none;"></a>
                    <?php endif; ?>
                <?php endforeach; ?>
                <div class="report_data"><?= Yii::app()->dateFormatter->format('d MMM yyyy г.', $row->date) ?></div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
