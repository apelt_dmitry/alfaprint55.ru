<div class="content_line_prices">
    <div class="price_top_line"animate='bounceInDown'>
    </div>
    <div class="left-price" animate='bounceInDown'>
        <div class="price-left">
        </div>
    </div>
    <div class="container">
        <div class="line_up_line">
        </div>
        <div class="price-slogan"animate='rubberBand'>
            Наши цены
        </div>
    </div>
    <div class="right-price"animate='bounceInDown'>
        <div class="price-right">
        </div>
    </div>
</div>
<div class="print_background">
    <div class="container">
        
        
        <?php foreach ($model as $val): ?>
        
        <div>
            <div class="print_name">
                <?=$val->name ?>
            </div>
            <div class="print_box">
                <div>
                    <table class="pr">
                    <tr>
                     <th>Материал</th><th>1-10 кв.м</th><th>10-30 кв.м</th><th>более 30 кв.м</th>
                    </tr>
                    <?php 
                    $mas = $val->Category_val;
                    foreach ($mas as $val2): 
                    ?>
                    <tr>
                     <td><?=$val2->material ?></td><td><?=$val2->price_1 ?></td><td><?=$val2->price_2 ?></td><td><?=$val2->price_3 ?></td>
                    </tr>
                    <?php endforeach; ?>
                   </table>
                </div>
            </div>
            <div class="sp_price">
                Цены действительны при условии подготовленных к печати файлов.<br/>
                Минимальная расчетная единица 0,5 кв.м. Цветопроба бесплатно.<br/> 
                Поля до 10 см – бесплатно
            </div>
        </div>
        
        <?php endforeach; ?>
        
        
        <div style="height: 100px;"></div>
    </div>
</div>