<?php
$cs = Yii::app()->clientScript;
$pt = Yii::app()->homeUrl;

$cs
    ->registerScriptFile('//api-maps.yandex.ru/2.1/?lang=ru_RU')
    ->registerScriptFile($pt.'js/icon_customImage.js');
?>

<div class="content_line_contacts">
    <div class="left_contacts">
        <div class="arrow_contacts_left"animate='bounceInLeft'></div>
        <div class="notebook_contacts"></div>
    </div>
    <div class="container">
        <div class="contacts-slogan1"animate='rubberBand'>
            Наши
        </div>
        <div class="contacts-slogan2"animate='rubberBand'>
            Контакты
        </div>
    </div>
    <div class="right_contacts">
        <div class="arrow_contacts_right"animate='bounceInRight'></div>
        <div class="page_contacts"></div>
    </div>
</div>
<div class="cotacts_area">
    <div class="container">
        <div class="contacts">
            <?php foreach ($model as $row):?>
            <div class="contact_box">
                <div class="photo1"style="background-size: cover; background-image: url('/uploads/contacts/preview/<?= $row->preview?>')"></div>
                <div class="contact_name"><?= $row->name ?></div>
                <div class="contact_post"><?= $row->position ?></div>
                <div class="contact_phone1"><?= $row->phone_1 ?></div>
                <div class="contact_phone2"><?= $row->phone_2 ?></div>
                <div class="contact_email"><?= $row->email ?></div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<div class="map_area">
    <div class="container">
        <div class="white_layer">
            <div id="map"></div>
        </div>
    </div>
</div>