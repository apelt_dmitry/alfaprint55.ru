<div class="news_background">
    <div class="news_news"></div>
    <div class="news_coffe"></div>
    <div class="news_micro"animate='flip'></div>
    <div class="news_slogan1"animate='flip'>наши</div>
    <div class="news_slogan2"animate='flip'>Новости</div>
    <div class="news_line"></div> 
    <div class="news_iphone"></div>
    <div class="news_ipad"></div>
</div>
<div class="news_body">
    <div class="container">
        <?php foreach ($model as $row):?>
        <div class="news_box">
            <div class="news_top"></div>
            <div class="news_bottom"></div>
            <div class="news_img" style="background: url('/uploads/news/<?= $row->preview?>') center no-repeat;"></div>
            <div class="news_title"><?= $row->title ?></div>
            <div class="news_description">
                <?= $row->description ?>
            </div>
            <a href="#" class="news_button" lol="cl<?=$row->id?>">Читать далее</a><div ></div>
            <div class="news_date"><?= Yii::app()->dateFormatter->format('d MMM yyyy г.', $row->date) ?></div>
        </div>
        <?php endforeach; ?>
        
    </div>
    <?php foreach ($model as $row):?>
    <div class="news_box_description show_hide cl<?=$row->id?>">
        <!--<div id="pre_line"></div>-->
        <div class="news_description_title"><div class="news_description_title2"><?= $row->title ?></div></div>
        <div class="news_description_title_line"></div>
        <div class="news_description_content"><?= $row->description ?></div>
    </div>
    <?php endforeach; ?>
</div>
<script>
    $(document).ready(function(){
       $('.news_button').click(function() {
        var cl='.';
        cl += $(this).attr('lol');
      $(cl).show();
      $('.news_box').addClass('show_hide');
      $('html, body').stop().animate({
        scrollLeft: 0, 
        scrollTop:$('.news_slogan1').offset().top
    }, 1000);  
      return false;
    });
    });
</script>
    