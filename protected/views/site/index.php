<head>
    <title> Главная </title>
</head>
<div class="content-line">
    <div class="left-promo">
        <div class="tee_hand " animate="bounceInLeft">
        </div>
        <div class="karandash" animate="bounceInLeft">
        </div>
        <div class="stiker" animate="bounceInLeft">
        </div>
        <div class="planshet" animate="bounceInLeft">
        </div>
    </div>
    <div class="container">
        <div class="promo" animate="bounceInDown">
            Создаём рекламу<br/>для вашего успеха
        </div>
    </div>
    <div class="right-promo">
        <div class="page_hand"animate="bounceInRight">
        </div>
        <div class="flashka"animate="bounceInRight">
        </div>
        <div class="marker"animate="bounceInRight">
        </div>
        <div class="mobila"animate="bounceInRight">
        </div>
        <div class="notebook" animate="bounceInRight">
        </div>
    </div>
</div>

<div class="wwd">
    <div class="red_line">

        <div class="white_line_middle">
        </div>
        <div class="wwd_text">
        Чем мы занимаемся
        </div>
        <div class="red_line_top">
        </div> 
        <div class="red_line_bottom">
        </div> 
    </div>
    <div class="container">
        <div class="box-wwd" animate='fadeInUpBig'>
            <div class="obj">
                <a href="/site/services?id=1" class="circle_1"></a>
                <div class="obj_text">Вывески</div>
            </div>
            <div class="obj">
                <a href="/site/services?id=4" class="circle_2"></a>
                <div class="obj_text">Широкофоматная печать</div>
            </div>
            <div class="obj">
                <a href="/site/services?id=11" class="circle_3"></a>
                <div class="obj_text">Печать на холсте</div>
            </div>
            <div class="obj">
                <a href="/site/services?id=12" class="circle_4"></a>
                <div class="obj_text">Стенды,таблички</div>
            </div>
            <div class="obj">
                <a href="/site/services?id=3" class="circle_5"></a>
                <div class="obj_text">Световые короба</div>
            </div>
            <div class="obj">
                <a href="/site/services?id=5" class="circle_6"></a>
                <div class="obj_text">Брандмауэры</div>
            </div>
            <div class="obj">
                <a href="/site/services?id=7" class="circle_7"></a>
                <div class="obj_text">Пилоны и стрелы</div>
            </div>
            <div class="obj">
                <a href="/site/services?id=8" class="circle_8"></a>
                <div class="obj_text">Штендеры</div>
            </div>
            <div class="obj">
                <a href="/site/services?id=9" class="circle_9"></a>
                <div class="obj_text">Фотообои</div>
            </div>
            <div class="obj">
                <a href="/site/services?id=10" class="circle_10"></a>
                <div class="obj_text">Брендирование транспорта</div>
            </div>
        </div>
    </div>
</div>

<div class="wwa">
    <div class="red_line2">

        <div class="white_line_middle2">
        </div>
        <div class="wwa_text" animate="flipInX">
        Наши клиенты
        </div>
        <div class="slider1" >
            <a id="prev" animate='bounceInLeft' href="#"></a>
            <div id="wrapper" animate='zoomIn'>
                <div id="carousel">
                    <?php foreach ($model as $row):?>
                    <div class="carousel_img"><table class="va"><tr><td><img src="/uploads/client/preview/<?= $row->preview?>"/></td></tr></table></div>
                    <?php endforeach;?>
                </div>
            </div>
            <a id="next" animate='bounceInRight' href="#"></a>
        </div>
        <div class="red_line_top">
        </div> 
        <div class="red_line_bottom">
        </div> 
    </div>
    <div class="ramki_bgr">
    </div>
    <div class="container">
        <div class="ramki">
            <div class="ramki_text">
                Наши благодарности
            </div>
            <div class="ramki_balagodarnosti">
                <img class="blagodarnosti_img" src="/img/blago_1.jpg">
                <img class="blagodarnosti_img" src="/img/blago_2.jpg">
                <img class="blagodarnosti_img" src="/img/blago_3.jpg">
                <img class="blagodarnosti_img" src="/img/blago_4.jpg">
                <img class="blagodarnosti_img" src="/img/blago_5.jpg">
            </div>
        </div>
    </div>
</div>

