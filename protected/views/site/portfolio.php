<?php
    $this->widget('application.extensions.fancybox.EFancyBox', array(
        'target'=>'a.gallery',
    ));
?>

<div class="portfolio_background">
    <div class="portfolio_img_1"animate='swing'></div>
    <div class="portfolio_img_2"animate='swing'></div>
    <div class="portfolio_img_3"animate='swing'></div>
    <div class="portfolio_img_4"animate='swing'></div>
    <div class="portfolio_img_5"animate='swing'></div>
    <div class="portfolio_img_6"animate='swing'></div>
    <div class="portfolio_img_7"animate='swing'></div>
    <div class="portfolio_img_8"animate='rotateInDownLeft'></div>
    <div class="portfolio_img_9"animate='rotateInDownRight'></div>
    <div class="portfolio_stars"></div>
    <div class="portfolio_slogan1">наше</div>
    <div class="portfolio_slogan2">портфолио</div>
    <div class="portfolio_line"></div> 
</div>
<div class="portfolio_background_selector">
    <div class="portfolio_selector">
        <script type="text/javascript">
            $(document).ready(function () {                
                var source = [
                    {value:'0',label:'ВСЕ'},
                 <?php foreach ($servicesModel as $row):?>
                    {value:'<?= $row->id ?>',label:'<?= $row->name ?>'},
                    <?php endforeach;?>
                        ];

                // Create a jqxDropDownList
                $("#jqxWidget").jqxDropDownList({ source: source, selectedIndex: 0, width: '270', height: '25',autoDropDownHeight: true});
                $('#jqxWidget').on('change', function (event) {     
                    var args = event.args;
                    if (args) {
                        // index represents the item's index.                      
                        var index = args.index;
                        var item = args.item;
                        // get item's label and value.
                        var label = item.label;
                        var value = item.value;
                    }
                    $('.hexagon_box').hide().removeClass('active');
                    if(value!=0){
                        $('.hexagon_box[service_id="'+value+'"]').addClass('active');
                        generation();
                        $('.hexagon_box[service_id="'+value+'"]').show();
                    }
                    else{
                        $('.hexagon_box').addClass('active');
                        generation();
                        $('.hexagon_box').show();
                    }
                    console.log(value);
                });
                /**/
                generation();
                /**/
            });
            function generation(){
                var x = 1;
                var y = 0.5;
                var t = false; // false - 3, true - 4
                var c = 0;
                $('.portfolio_container').css({
                    height: 460*Math.ceil($('.portfolio_container .hexagon_box.active').length/7)
                });
                $('.portfolio_container .hexagon_box.active').each(function(key){
                    console.log(x, y, t, c);
                    $(this).css({
                        left: x*253-211,
                        top: y*210-150
                    });
                    if ( t==false && c==2 ) {
                        c = 0;
                        t = true;
                        y++;
                        x = 0.5;
                    } else if ( t==true && c==3 ) {
                        c = 0;
                        t = false;
                        y++;
                        x = 1;
                    } else {
                        c++;
                        x++;
                    }
                });
            }
        </script>
        <div id='jqxWidget'></div>
    </div>
</div>
<div class="portfolio_box">
    <div class="container">
        <div class="portfolio_container">
            <?php foreach ($model as $row):?>
            <div class="hexagon_box active" service_id="<?=$row->service_id?>">
                <?php
                    $slides =$row->portfolioSlide;
                    $firstSlide=true;
                ?>
                
                <?php foreach ($slides as $slide):?>
                    <?php if ($firstSlide): ?>
                        <?php $firstSlide=false;?>
                        <a rel="portfolio_<?= $row->id ?>" href="/uploads/portfolio/<?= $slide->img ?>" class="gallery b-polygon b-polygon_hexagon b-polygon_hexagon2">
                            <span class="b-polygon-part">
                                <span class="b-polygon-part b-polygon-part_content" style="background: url('/uploads/portfolio/preview/<?=$slide->img ?>') center no-repeat;" >
                                </span>
                            </span>
                        </a>
                    <?php else:?>
                        <a class="gallery" rel="portfolio_<?= $row->id ?>" href="/uploads/portfolio/<?= $slide->img ?>" style="display: none;"></a>
                    <?php endif;?>
                <?php endforeach; ?>
            </div>
            <?php endforeach;?>
        </div>
    </div> 
</div>