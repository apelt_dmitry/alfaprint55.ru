<?php

Class AdminController extends Controller
{

    public $layout='/layouts/main';


    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    
    public function accessRules()
    {
        return array(
            array('allow',
                'controllers'=>array('default'),
                'actions'=>array('login'),
                'users'=>array('*'),
            ),
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
                'deniedCallback' => function() { Yii::app()->controller->redirect(array('/admin/default/login')); },
            ),
        );
    }

    
    /*public function init()
    {
        parent::init();

    }*/
    
    
}