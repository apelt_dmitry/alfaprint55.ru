<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'menu-form',
    'enableAjaxValidation'=>false,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnChange'=>false,
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
)); ?>
    

    <?= $form->fileFieldControlGroup($model,'image'); ?>

    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->textFieldControlGroup($model,'title',array('maxlenght'=>24)); ?>
    <div class="form-group">
        <?= $form->labelEx($model, 'description', array(
            'class'=>'control-label col-lg-2',
        )) ?>
        <div class="col-lg-10">
            <?php $this->widget('ext.ckeditor.CKEditorWidget', array(
                'model' => $model,
                'attribute' => 'description',
                'defaultValue' => $model->description,
                'config' => array(
                    //'height' => '400px',
                    //'width' => '100%',
                ),
            )); ?>
        </div>
    </div>  
    <?php echo $form->textFieldControlGroup($model,'date'); ?>
    <?php //echo $form->dateField($model, 'data', array('placeholder' => 'dateField'));?>

    <?= BsHtml::formActions(array(
        BsHtml::resetButton('Сброс', array(
            'color' => BsHtml::BUTTON_COLOR_WARNING,
            'icon' => BsHtml::GLYPHICON_REFRESH,
        )),
        BsHtml::submitButton('Готово', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
        )),
    ), array('class'=>'form-actions')); ?>

<?php $this->endWidget(); ?>




<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    //'enableAjaxValidation' => true,
    //'id' => 'user_form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
));
?>
<?php
$this->endWidget();
?>

