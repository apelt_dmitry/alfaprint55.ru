<?php echo BsHtml::pageHeader('Список страниц') ?>


<?= BsHtml::linkButton('Менеджер категорий', array(
    'icon' => BsHtml::GLYPHICON_OK,
    'color' => BsHtml::BUTTON_COLOR_SUCCESS,
    'url' => array('/admin/priceCategory'),
    //'target'=>'_blank',
    //'block' => true,
    'style'=>'float: right;',
)); ?>

<?= BsHtml::linkButton('Добавить цену', array(
    'icon' => BsHtml::GLYPHICON_PLUS,
    'color' => BsHtml::BUTTON_COLOR_SUCCESS,
    'url' => array('/admin/price/create'),
    //'target'=>'_blank',
    //'block' => true,
    'style'=>'float: right;margin-right:10px;',
)); ?>

<?php $this->widget('bootstrap.widgets.BsGridView',array(
    'id'=>'menu-grid',
    'dataProvider'=>$model->search(),
    'filter'=>null,
    
    'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
    'template' => '{summary}{items}{pager}',
    'pager'=>array(
        'class' => 'bootstrap.widgets.BsPager',
        'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
    ),
    'nullDisplay'=>'-',
    'selectableRows'=>0,
    
    'columns'=>array(
       
        array(
            'header' => 'Категории',
            'value' => '$data->categoryName->name'
        ),
        'material',
        'price_1',
        'price_2',
        'price_3',
        array(
            'class'=>'bootstrap.widgets.BsButtonColumn',
            'template' => '{update}{delete}',
        ),
    ),
)); ?>