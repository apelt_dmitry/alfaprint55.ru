<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'menu-form',
    'enableAjaxValidation'=>false,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnChange'=>false,
        'validateOnSubmit'=>true,
    ),
)); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListControlGroup($model,'category', PriceCategory::model()->_dropDown, array(
        //'empty' => 'no',
        //'help'=>'Для создания подпунктов - выберите родителя.',
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'material'); ?>
    <?php echo $form->textFieldControlGroup($model,'price_1'); ?>
    <?php echo $form->textFieldControlGroup($model,'price_2'); ?>
    <?php echo $form->textFieldControlGroup($model,'price_3'); ?>
    
    

    <?= BsHtml::formActions(array(
        BsHtml::resetButton('Сброс', array(
            'color' => BsHtml::BUTTON_COLOR_WARNING,
            'icon' => BsHtml::GLYPHICON_REFRESH,
        )),
        BsHtml::submitButton('Готово', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
        )),
    ), array('class'=>'form-actions')); ?>

<?php $this->endWidget(); ?>
