<?php echo BsHtml::pageHeader('Портфолио') ?>


<?= BsHtml::linkButton('Добавить изображение', array(
    'icon' => BsHtml::GLYPHICON_PLUS,
    'color' => BsHtml::BUTTON_COLOR_SUCCESS,
    'url' => array('/admin/portfolio/create'),
    //'target'=>'_blank',
    //'block' => true,
    'style'=>'float: right;margin-right:10px;',
)); ?>

<?php $this->widget('bootstrap.widgets.BsGridView',array(
    'id'=>'menu-grid',
    'dataProvider'=>$model->search(),
    'filter'=>null,
    
    'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
    'template' => '{summary}{items}{pager}',
    'pager'=>array(
        'class' => 'bootstrap.widgets.BsPager',
        'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
    ),
    'nullDisplay'=>'-',
    'selectableRows'=>0,
    
    'columns'=>array(
       
        array(
            'header' => 'Изображение',
            'type'=>'raw',
            'value' =>'$data->_preview',
        ),
        array(
            'name' => 'service_id',
            //'type'=>'raw',
            'value'=>'$data->services->name',
        ),
        array(
            'class'=>'bootstrap.widgets.BsButtonColumn',
            'template' => '{update}{delete}',
        ),
    ),
)); ?>