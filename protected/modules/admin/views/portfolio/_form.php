<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'menu-form',
    'enableAjaxValidation'=>false,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnChange'=>false,
        'validateOnSubmit'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
)); ?>
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->dropDownListControlGroup($model,'service_id', Services::model()->_dropDownList, array(
            //'empty' => 'no',
    )); ?>
    <?= $form->fileFieldControlGroup($model,'image[]',array(
            'help'=>'До 12 изображений любого размера.'.( (!$model->isNewRecord)?' Выбор новых изображений удалит текущие (кол-во: '.count($model->services).').':'' ),
                'multiple'=>'multiple',
        )); ?>

    <?php //echo $form->dateField($model, 'data', array('placeholder' => 'dateField'));?>

    <?= BsHtml::formActions(array(
        BsHtml::resetButton('Сброс', array(
            'color' => BsHtml::BUTTON_COLOR_WARNING,
            'icon' => BsHtml::GLYPHICON_REFRESH,
        )),
        BsHtml::submitButton('Готово', array(
            'color' => BsHtml::BUTTON_COLOR_SUCCESS,
            'icon' => BsHtml::GLYPHICON_OK,
        )),
    ), array('class'=>'form-actions')); ?>

<?php $this->endWidget(); ?>




<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    //'enableAjaxValidation' => true,
    //'id' => 'user_form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>true,
    ),
    'htmlOptions' => array(
        'class' => 'bs-example',
        'enctype'=>'multipart/form-data',
    )
));
?>
<?php
$this->endWidget();
?>

