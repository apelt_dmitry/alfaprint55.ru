<?php echo BsHtml::pageHeader('Услуги') ?>

<?= BsHtml::linkButton('Добавить страницу', array(
    'icon' => BsHtml::GLYPHICON_PLUS,
    'color' => BsHtml::BUTTON_COLOR_SUCCESS,
    'url' => array('/admin/services/create'),
    //'target'=>'_blank',
    //'block' => true,
    'style'=>'float: right;margin-right:10px;',
)); ?>

<?php $this->widget('bootstrap.widgets.BsGridView',array(
    'id'=>'menu-grid',
    'dataProvider'=>$model->search(),
    'filter'=>null,
    
    'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
    'template' => '{summary}{items}{pager}',
    'pager'=>array(
        'class' => 'bootstrap.widgets.BsPager',
        'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
    ),
    'nullDisplay'=>'-',
    'selectableRows'=>0,
    
    'columns'=>array(
       
        array(
            'header' => 'Имя',
            'value' => '$data->name'
        ),
        array(
            'name' => 'img',
            'type'=>'raw',
            'value' => 'BsHtml::image("/uploads/service/preview/".$data->img)',
        ),
        'text',
        'text_promo_1',
        'text_promo_2',
        array(
            'class'=>'bootstrap.widgets.BsButtonColumn',
            'template' => '{update}{delete}',
        ),
    ),
)); ?>