<?php echo BsHtml::pageHeader('Фотоотчёты') ?>


<?= BsHtml::linkButton('Добавить  фотоотчёт', array(
    'icon' => BsHtml::GLYPHICON_PLUS,
    'color' => BsHtml::BUTTON_COLOR_SUCCESS,
    'url' => array('/admin/report/create'),
    //'target'=>'_blank',
    //'block' => true,
    'style'=>'float: right;margin-right:10px;',
)); ?>


<?php $this->widget('bootstrap.widgets.BsGridView',array(
    'id'=>'menu-grid',
    'dataProvider'=>$model->search(),
    'filter'=>null,
    
    'type' => BsHtml::GRID_TYPE_HOVER/*. ' ' . BsHtml::GRID_TYPE_CONDENSED*/,
    'template' => '{summary}{items}{pager}',
    'pager'=>array(
        'class' => 'bootstrap.widgets.BsPager',
        'size'=>BsHtml::PAGINATION_SIZE_DEFAULT,
    ),
    'nullDisplay'=>'-',
    'selectableRows'=>0,
    
    'columns'=>array(
        array(
            'name' => 'preview',
            'type'=>'raw',
            'value' => 'BsHtml::image("/uploads/report/preview/".$data->preview)',
        ),
        'title',
        array(
            'name' => 'description',
            'type'=>'raw',
        ),
        array(
            'name' => 'date',
            'value' => 'Yii::app()->dateFormatter->format(\'d MMMM yyyy г.\', $data->date)',
        ),
        
        array(
            'class'=>'bootstrap.widgets.BsButtonColumn',
            'template' => '{update}{delete}',
        ),
    ),
)); ?>