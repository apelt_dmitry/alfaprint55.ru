<?php

class ContactsController extends AdminController
{

    
    
    public function actionCreate()
    {
    $model = new Contacts();
        
        if ( isset($_POST['Contacts']) ) {
            $model->attributes = $_POST['Contacts'];
            $model->image=CUploadedFile::getInstance($model,'image');
            
            if($model->save()){
                    $file = $model->image;
                    if ( $file->name!='' ) {
                        $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                        $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                        $image = Yii::app()->image->load($file->tempName);
                        $image->save('./uploads/contacts/'.$imageName);
                        $image->resize(256, 256);
                        $image->save('./uploads/contacts/preview/'.$imageName);
                        $model->preview = $imageName;
                        $model->save(FALSE);
                    }
                
                $this->redirect(array('index'));
            };
        }
        $this->render('create', array(
            'model'=>$model,
        ));
    }

    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if ( isset($_POST['Contacts']) ) {
            $model->attributes = $_POST['Contacts'];
            $model->image=CUploadedFile::getInstance($model,'image');

            if($model->save()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/contacts/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/contacts/preview/'.$imageName);
                    $model->preview = $imageName;
                    $model->save(FALSE);
                }
                $this->redirect(array('index'));
            };
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new Contacts('search');
        $model->unsetAttributes();
        if ( isset($_GET['Contacts']) ) {
            $model->attributes=$_GET['Contacts'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Contacts::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
}
