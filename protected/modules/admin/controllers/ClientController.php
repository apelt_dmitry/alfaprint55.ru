<?php

class ClientController extends AdminController
{

    
    
    public function actionCreate()
    {
    $model = new Client();
        
        if ( isset($_POST['Client']) ) {
            $model->attributes = $_POST['Client'];
            $model->image=CUploadedFile::getInstance($model,'image');
            
            if($model->save()){
                    $file = $model->image;
                    if ( $file->name!='' ) {
                        $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                        $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                        $image = Yii::app()->image->load($file->tempName);
                        $image->save('./uploads/client/'.$imageName);
                        $image->resize(256, 256);
                        $image->save('./uploads/client/preview/'.$imageName);
                        $model->preview = $imageName;
                        $model->save(FALSE);
                    }
                
                $this->redirect(array('index'));
            };
        }
        $this->render('create', array(
            'model'=>$model,
        ));
    }

    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if ( isset($_POST['Client']) ) {
            $model->attributes = $_POST['Client'];
            $model->image=CUploadedFile::getInstance($model,'image');

            if($model->save()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/client/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/client/preview/'.$imageName);
                    $model->preview = $imageName;
                    $model->save(FALSE);
                }
                $this->redirect(array('index'));
            };
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new Client('search');
        $model->unsetAttributes();
        if ( isset($_GET['Client']) ) {
            $model->attributes=$_GET['Client'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Client::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
}
