<?php

class ServicesController extends AdminController
{

    
    public function actionCreate()
    {
        $model = new Services();
        
        if ( isset($_POST['Services']) ) {
            $model->attributes = $_POST['Services'];
            $model->image=CUploadedFile::getInstance($model,'image');
            
            if($model->validate()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/service/'.$imageName);
                    $image->resize(512, 512);
                    $image->save('./uploads/service/preview/'.$imageName);
                    $model->img = $imageName;
                }
                $model->save(FALSE);
                $this->redirect(array('index'));
                
            };
        }
        $this->render('create', array(
            'model'=>$model,
        ));
    }

    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

            if ( isset($_POST['Services']) ) {
            $model->attributes = $_POST['Services'];
            $model->image=CUploadedFile::getInstance($model,'image');
            
            if($model->validate()){
                $file = $model->image;
                if ( $file->name!='' ) {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName      = substr(md5($file->name.microtime()), 0, 28).'.'.$imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/services/'.$imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/services/preview/'.$imageName);
                    $model->img = $imageName;
                    
                }
                $model->save(FALSE);
                $this->redirect(array('index'));
            };
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new Services('search');
        $model->unsetAttributes();
        if ( isset($_GET['Services']) ) {
            $model->attributes=$_GET['Services'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Services::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    
}
