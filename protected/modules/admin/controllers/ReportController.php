<?php

class ReportController extends AdminController
{


    public function actionCreate()
    {
        $model = new Report();

        if (isset($_POST['Report'])) {
            $model->attributes = $_POST['Report'];
            $model->date = strtotime($model->date);
            $model->image = CUploadedFile::getInstance($model, 'image');
            $model->slides = CUploadedFile::getInstances($model, 'slides');

            if ($model->validate()) {
                $file = $model->image;
                if ($file->name != '') {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName = substr(md5($file->name . microtime()), 0, 28) . '.' . $imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/report/' . $imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/report/preview/' . $imageName);
                    $model->preview = $imageName;
                }
                if ($model->save(FALSE)) {
                    if (count($model->slides) != 0) {
                        foreach ($model->slides as $file) {
                            if ($file->name != '') {
                                $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                                $imageName = substr(md5($file->name . microtime()), 0, 28) . '.' . $imageExtention;
                                $image = Yii::app()->image->load($file->tempName);
                                $image->save('./uploads/report/' . $imageName);
                                $image->resize(256, 256);
                                $image->save('./uploads/report/preview/' . $imageName);
                                $reportSlide = new ReportSlide();
                                $reportSlide->report_id = $model->id;
                                $reportSlide->img = $imageName;
                                $reportSlide->save();
                            }
                        }
                    }
                    $this->redirect(array('index'));
                }
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }


    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $model->date = date('d.m.Y', $model->date);
        if (isset($_POST['Report'])) {
            $model->attributes = $_POST['Report'];
            $model->date = strtotime($model->date);
            $model->image = CUploadedFile::getInstance($model, 'image');
            $model->slides = CUploadedFile::getInstances($model, 'slides');

            if ($model->validate()) {
                $file = $model->image;
                if ($file->name != '') {
                    $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                    $imageName = substr(md5($file->name . microtime()), 0, 28) . '.' . $imageExtention;
                    $image = Yii::app()->image->load($file->tempName);
                    $image->save('./uploads/report/' . $imageName);
                    $image->resize(256, 256);
                    $image->save('./uploads/report/preview/' . $imageName);
                    $model->preview = $imageName;
                }
                if ($model->save(FALSE)) {
                    if (count($model->slides) != 0) {
                        foreach ($model->slides as $file) {
                            if ($file->name != '') {
                                $imageExtention = pathinfo($file->getName(), PATHINFO_EXTENSION);
                                $imageName = substr(md5($file->name . microtime()), 0, 28) . '.' . $imageExtention;
                                $image = Yii::app()->image->load($file->tempName);
                                $image->save('./uploads/report/' . $imageName);
                                $image->resize(256, 256);
                                $image->save('./uploads/report/preview/' . $imageName);
                                $reportSlide = new ReportSlide();
                                $reportSlide->report_id = $model->id;
                                $reportSlide->img = $imageName;
                                $reportSlide->save();
                            }
                        }
                    }

                    $this->redirect(array('index'));
                }
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }


    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new Report('search');
        $model->unsetAttributes();
        if (isset($_GET['Report'])) {
            $model->attributes = $_GET['Report'];
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }


    public function loadModel($id)
    {
        $model = Report::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'menu-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


}
