<?php

class PriceController extends AdminController
{

    
    public function actionCreate()
    {
        $model = new Price();               
        if ( isset($_POST['Price']) ) {
            $model->attributes = $_POST['Price'];
            if( $model->save() ) {
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(       
            'model'=>$model,
        ));
    }

    
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if ( isset($_POST['Price']) ) {
            $model->attributes = $_POST['Price'];
            if( $model->save() ) {
                $this->redirect(array('index'));
            }   
        }

        $this->render('update', array(
            'model'=>$model,
        ));
    }


    public function actionDelete($id)
    {
        
        $this->loadModel($id)->delete();

        if ( !isset($_GET['ajax']) ) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    public function actionIndex()
    {
        $model = new Price('search');
        $model->unsetAttributes();
        if ( isset($_GET['Price']) ) {
            $model->attributes=$_GET['Price'];
        }

        $this->render('index', array(
            'model'=>$model,
        ));
    }


    public function loadModel($id)
    {
        $model=Price::model()->findByPk($id);
        if ( $model===null ) {
            throw new CHttpException(404,'The requested page does not exist.');
        }
                
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if ( isset($_POST['ajax']) && $_POST['ajax']==='menu-form' ) {
          
            Yii::app()->end();
        }
    }
    
    
}
