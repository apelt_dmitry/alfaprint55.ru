ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [54.999246, 73.347391],
            zoom: 16,
            controls: []
        });
        myMap.behaviors.disable('scrollZoom');
        
        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'Офис Альфа-принт'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: '/img/loc_4.png',
            // Размеры метки.
            iconImageSize: [140, 160],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [150, -180],
            
        });
 
    myMap.geoObjects.add(myPlacemark);
     
        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'Производство Альфа-принт'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: '/img/loc_7.png',
            // Размеры метки.
            iconImageSize: [111, 103],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-40, -100]
        });

    myMap.geoObjects.add(myPlacemark);
  //  YMaps.Events.observe(function () {
 //       map.removeControl(typecontrol);
 //   });
});
